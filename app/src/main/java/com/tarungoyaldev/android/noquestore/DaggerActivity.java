package com.tarungoyaldev.android.noquestore;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.tarungoyaldev.android.noquestore.Login.LoginActivity;

import javax.inject.Inject;

import dagger.ObjectGraph;

/**
 * Created by tarungoyal on 13/07/16.
 */
public abstract class DaggerActivity extends AppCompatActivity {

    protected ObjectGraph mObjectGraph;

    protected abstract Object getModule();

    @Inject
    Config config;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        createObjectGraphIfNeeded();
        mObjectGraph.inject(this);
        if (config.getStoreId() == 0 && ((App) getApplication()).initiateStoreLogin() == false
                && !(this instanceof  LoginActivity)) {
            Intent intent = new Intent(this, LoginActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        }
    }

    protected void createObjectGraphIfNeeded() {
        if (mObjectGraph == null) {
            mObjectGraph = ((App) getApplication()).createScopedGraph(getModule());
        }
    }

    public void inject(Object object) {
        createObjectGraphIfNeeded();
        mObjectGraph.inject(object);
    }
}
