package com.tarungoyaldev.android.noquestore.Login;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import com.tarungoyaldev.android.noquestore.App;
import com.tarungoyaldev.android.noquestore.DaggerActivity;
import com.tarungoyaldev.android.noquestore.R;
import com.tarungoyaldev.android.noquestore.StoreHome.StoreHomeActivity;
import com.tarungoyaldev.android.noquestore.StoreService;

import java.util.HashMap;

import javax.inject.Inject;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class LoginActivity extends DaggerActivity {

    private static final String TAG = "LoginActivity";

    @Inject
    StoreService storeService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_login);
        super.onCreate(savedInstanceState);
        Log.e("LoginActivity", "!!!!!!!Login View!!!!!!");
    }

    @Override
    protected Object getModule() {
        return new LoginModule(this);
    }

    public void login(View view) {
        storeService.loginStoreManager(getLoginBody())
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(storeManager -> {
                    ((App) getApplication()).setStoreManager(storeManager);
                    Intent intent = new Intent(LoginActivity.this, StoreHomeActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                }, error -> {
                    Log.e(TAG, error.toString());
                });
    }

    private HashMap<String, String> getLoginBody() {
        HashMap<String, String> loginHash = new HashMap<>();
        String username = ((EditText) findViewById(R.id.usernameEditText)).getText().toString();
        String password = ((EditText) findViewById(R.id.passwordEditText)).getText().toString();
        loginHash.put("username", username);
        loginHash.put("password", password);
        return loginHash;
    }
}
