package com.tarungoyaldev.android.noquestore.SplashScreen;


import com.tarungoyaldev.android.noquestore.AppModule;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by tarungoyal on 12/07/16.
 */
@Module(
        injects = {
                SplashScreenActivity.class,
        },
        addsTo = AppModule.class
)
public class SplashScreenModule {
    private SplashScreenActivity splashScreenActivity;

    public SplashScreenModule(SplashScreenActivity splashScreenActivity) {
        this.splashScreenActivity = splashScreenActivity;
    }

    @Provides
    @Singleton
    public SplashScreenActivity provideSplashScreenActivity() {
        return splashScreenActivity;
    }
}
