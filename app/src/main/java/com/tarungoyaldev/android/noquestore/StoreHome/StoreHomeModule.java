package com.tarungoyaldev.android.noquestore.StoreHome;

import com.tarungoyaldev.android.noquestore.AppModule;
import com.tarungoyaldev.android.noquestore.model.StoreProduct;

import java.util.ArrayList;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by tarungoyal on 12/07/16.
 */
@Module(
        injects = {
                StoreHomeActivity.class,
//                StoreDetailsFragment.class,
//                StoreProductFragment.class
        },
        library = true,
        addsTo = AppModule.class
)
public class StoreHomeModule {
    private StoreHomeActivity storeHomeActivity;

    public StoreHomeModule(StoreHomeActivity storeHomeActivity) {
        this.storeHomeActivity = storeHomeActivity;
    }

    @Provides
    @Singleton
    public StoreHomeActivity provideStoreDetailsActivity() {
        return storeHomeActivity;
    }

    @Provides
    @Singleton
    public ArrayList<StoreProduct> provideStoreProductList() {
        return new ArrayList<StoreProduct>();
    }
}
