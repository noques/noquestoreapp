package com.tarungoyaldev.android.noquestore;

import com.google.repacked.antlr.v4.runtime.misc.Nullable;
import com.tarungoyaldev.android.noquestore.model.StoreLogin;

/**
 * Created by tarungoyal on 22/07/16.
 */
public class Config {

    private String baseUrl = "http://noques.com/";

    private StoreLogin storeLogin;

    public Config() {

    }

    /**
     * @return storeId if present, 0 otherwise.
     */
    public long getStoreId() {
        if (storeLogin != null) {
            return storeLogin.getStoreId();
        } else {
            return 0;
        }
    }

    public String getBaseUrl() {
        return baseUrl;
    }

    public void setStoreLogin(@Nullable StoreLogin storeLogin) {
        this.storeLogin = storeLogin;
    }

    @Nullable
    public StoreLogin getStoreLogin() {
        return storeLogin;
    }
}
