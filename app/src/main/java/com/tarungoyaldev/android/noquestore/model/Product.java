package com.tarungoyaldev.android.noquestore.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by tarungoyal on 15/07/16.
 */
public class Product implements Serializable {

    @SerializedName("id")
    @Expose
    private Long id;
    @SerializedName("product_name")
    @Expose
    private String productName;
    @SerializedName("short_description")
    @Expose
    private String shortDescription;
    @SerializedName("maximum_retail_price")
    @Expose
    private Double maximumRetailPrice;
    @SerializedName("barcode_number")
    @Expose
    private Long barcodeNumber;
    @SerializedName("image_url")
    @Expose
    private String imageUrl;
    @SerializedName("category")
    @Expose
    private String category;

    /**
     *
     * @return
     * The id
     */
    public Long getId() {
        return id;
    }

    /**
     *
     * @param id
     * The id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     *
     * @return
     * The productName
     */
    public String getProductName() {
        return productName;
    }

    /**
     *
     * @param productName
     * The product_name
     */
    public void setProductName(String productName) {
        this.productName = productName;
    }

    /**
     *
     * @return
     * The shortDescription
     */
    public String getShortDescription() {
        return shortDescription;
    }

    /**
     *
     * @param shortDescription
     * The short_description
     */
    public void setShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
    }

    /**
     *
     * @return
     * The maximumRetailPrice
     */
    public Double getMaximumRetailPrice() {
        return maximumRetailPrice;
    }

    /**
     *
     * @param maximumRetailPrice
     * The maximum_retail_price
     */
    public void setMaximumRetailPrice(Double maximumRetailPrice) {
        this.maximumRetailPrice = maximumRetailPrice;
    }

    /**
     *
     * @return
     * The barcodeNumber
     */
    public Long getBarcodeNumber() {
        return barcodeNumber;
    }

    /**
     *
     * @param barcodeNumber
     * The barcode_number
     */
    public void setBarcodeNumber(Long barcodeNumber) {
        this.barcodeNumber = barcodeNumber;
    }

    /**
     *
     * @return
     * The imageUrl
     */
    public String getImageUrl() {
        return imageUrl.length() > 0 ? imageUrl : null;
    }

    /**
     *
     * @param imageUrl
     * The image_url
     */
    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl.trim().length() > 0 ? imageUrl.trim() : null;
    }

    /**
     *
     * @return
     * The category
     */
    public String getCategory() {
        return category;
    }

    /**
     *
     * @param category
     * The category
     */
    public void setCategory(String category) {
        this.category = category;
    }
}
