package com.tarungoyaldev.android.noquestore.CustomerReceipt;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.ListFragment;
import android.util.Log;

import com.google.gson.Gson;
import com.tarungoyaldev.android.noquestore.Config;
import com.tarungoyaldev.android.noquestore.StoreService;
import com.tarungoyaldev.android.noquestore.barcode.BarcodeCaptureActivity;
import com.tarungoyaldev.android.noquestore.model.Order;
import com.tarungoyaldev.android.noquestore.model.StoreProduct;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * A simple {@link Fragment} subclass.
 */
public class ReceiptItemListFragment extends ListFragment {

    @Inject
    Config config;
    @Inject
    StoreService storeService;
    @Inject
    Gson gson;

    private ReceiptUpdateListener receiptUpdateListener;
    public static final String TAG = "CustomerReceiptActivity";

    public interface ReceiptUpdateListener {
        void updateTotalAmount(int amount);
    }

    public ReceiptItemListFragment() {
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ((CustomerReceiptActivity) getActivity()).inject(this);
        List<StoreProduct> storeProducts = new ArrayList<>();
        RecieptItemListAdapter adapter = new RecieptItemListAdapter(getContext(), storeProducts);
        setListAdapter(adapter);

        Observable<Order> storeProListObservable  =
                storeService.getOrder(getArguments().getString(BarcodeCaptureActivity.BarcodeObject));

        receiptUpdateListener = (CustomerReceiptActivity) getActivity();

        storeProListObservable.subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(order -> {
                    System.out.println("Store Product serialized: " + gson.toJson(order));
                    updateRecieptItems(order);
                }, error -> {
                    Log.e(TAG, error.toString());
                });
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private void updateRecieptItems(Order order) {
        RecieptItemListAdapter recieptItemListAdapter = (RecieptItemListAdapter) getListAdapter();
        recieptItemListAdapter.addItems(order.getStoreProducts());
        int totalAmount = 0;
        for (StoreProduct storeProduct : order.getStoreProducts()) {
            totalAmount += storeProduct.getStorePrice() * storeProduct.getQuantityAvailable();
        }
        receiptUpdateListener.updateTotalAmount(totalAmount);
    }
}
