package com.tarungoyaldev.android.noquestore.model;

import com.orm.SugarRecord;

/**
 * Created by tarungoyal on 22/07/16.
 */
public class LocalStoreProduct extends SugarRecord {
    String name;
    Integer sellingPrice;
    Integer costPrice;
    Integer quantity;
    String barcode;

    public LocalStoreProduct(){
    }

    public LocalStoreProduct(String name, Integer sellingPrice, Integer costPrice, Integer quantity,
                             String barcode){
        this.name = name;
        this.sellingPrice = sellingPrice;
        this.costPrice = costPrice;
        this.quantity = quantity;
        this.barcode = barcode;
    }

    public String getName() {
        return name;
    }

    public Integer getSellingPrice() {
        return sellingPrice;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSellingPrice(Integer sellingPrice) {
        this.sellingPrice = sellingPrice;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public Integer getCostPrice() {
        return costPrice;
    }

    public void setCostPrice(Integer costPrice) {
        this.costPrice = costPrice;
    }

}
