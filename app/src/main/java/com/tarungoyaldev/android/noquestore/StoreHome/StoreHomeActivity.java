package com.tarungoyaldev.android.noquestore.StoreHome;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.google.gson.Gson;
import com.tarungoyaldev.android.noquestore.App;
import com.tarungoyaldev.android.noquestore.Config;
import com.tarungoyaldev.android.noquestore.CustomerReceipt.CustomerReceiptActivity;
import com.tarungoyaldev.android.noquestore.DaggerActivity;
import com.tarungoyaldev.android.noquestore.Login.LoginActivity;
import com.tarungoyaldev.android.noquestore.R;
import com.tarungoyaldev.android.noquestore.StoreItemList.StoreItemListActivity;
import com.tarungoyaldev.android.noquestore.StoreService;
import com.tarungoyaldev.android.noquestore.barcode.BarcodeCaptureActivity;
import com.tarungoyaldev.android.noquestore.model.StoreInformation;

import javax.inject.Inject;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class StoreHomeActivity extends DaggerActivity {

    private static final String TAG = "StoreDetailActivity";
    private TextView storeName;
    private TextView storeLocation;

    @Inject
    Config config;
    @Inject
    StoreService storeService;
    @Inject
    Gson gson;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_store_home);
        super.onCreate(savedInstanceState);

//        System.out.println("Store Product serialized: " + gson.toJson(storeProduct1));

        storeName = (TextView) findViewById(R.id.storeNameText);
        storeLocation = (TextView) findViewById(R.id.storeCityText);

        Long storeId = config.getStoreId();
        if (storeId == 0) {
            Intent intent = new Intent(this, LoginActivity.class);
            startActivity(intent);
            return;
        }
        Observable<StoreInformation> storeInformationObservable =
                storeService.getStoreInformation(storeId);

        storeInformationObservable.subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(storeInformation -> {
                    storeName.setText(storeInformation.getStoreName());
                    storeLocation.setText(storeInformation.getCity());
                }, error -> {
                    Log.e(TAG, error.toString());
                });
    }

    @Override
    protected Object getModule() {
        return new StoreHomeModule(this);
    }

    public void logout(View view) {
        System.out.println("In Logout!!!!!");
        ((App) getApplication()).logout();
        Intent intent = new Intent(this, LoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    public void updateInventory(View view) {
        Intent intent = new Intent(StoreHomeActivity.this, StoreItemListActivity.class);
        startActivity(intent);
    }

    public void userQRCodeScanned(View view) {
        Intent intent = new Intent(StoreHomeActivity.this, BarcodeCaptureActivity.class);
        intent.putExtra(BarcodeCaptureActivity.AutoFocus, true);
        intent.putExtra(BarcodeCaptureActivity.UseFlash, false);
        intent.putExtra(BarcodeCaptureActivity.ResultActivity, CustomerReceiptActivity.class);
        startActivity(intent);
    }
}
