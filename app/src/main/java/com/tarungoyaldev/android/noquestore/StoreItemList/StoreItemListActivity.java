package com.tarungoyaldev.android.noquestore.StoreItemList;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.Toolbar;

import com.tarungoyaldev.android.noquestore.DaggerActivity;
import com.tarungoyaldev.android.noquestore.R;
import com.tarungoyaldev.android.noquestore.StoreItemDetails.StoreItemDetailsActivity;
import com.tarungoyaldev.android.noquestore.barcode.BarcodeCaptureActivity;
import com.tarungoyaldev.android.noquestore.model.StoreProduct;

public class StoreItemListActivity extends DaggerActivity
        implements StoreItemListFragment.StoreProductUpdateListener {

    private static final String TAG = "StoreItemListActivity";

    private FloatingActionButton addProductFab;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_store_item_list);
        super.onCreate(savedInstanceState);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        addProductFab = (FloatingActionButton) findViewById(R.id.add_product_fab);
        if (addProductFab != null) {
            addProductFab.setActivated(false);
            addProductFab.setOnClickListener(view -> {
                Intent intent = new Intent(StoreItemListActivity.this, BarcodeCaptureActivity.class);
                intent.putExtra(BarcodeCaptureActivity.AutoFocus, true);
                intent.putExtra(BarcodeCaptureActivity.UseFlash, false);
                intent.putExtra(BarcodeCaptureActivity.ResultActivity, StoreItemDetailsActivity.class);
                startActivity(intent);
            });
        }

        if (findViewById(R.id.storeMainLayout) != null) {

            if (savedInstanceState != null) {
                return;
            }

            StoreItemListFragment storeItemListFragment = new StoreItemListFragment();

            storeItemListFragment.setArguments(getIntent().getExtras());

            getSupportFragmentManager().beginTransaction()
                    .add(R.id.storeMainLayout, storeItemListFragment).commit();
        }
    }

    @Override
    protected Object getModule() {
        return new StoreItemListModule(this);
    }

    @Override
    public void addStoreProduct(StoreProduct storeProduct) {

    }

    @Override
    public void storeListFetched() {
        addProductFab.setActivated(true);
    }
}
