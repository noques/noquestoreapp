package com.tarungoyaldev.android.noquestore.StoreItemList;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.View;
import android.widget.ListView;

import com.tarungoyaldev.android.noquestore.Config;
import com.tarungoyaldev.android.noquestore.StoreItemDetails.StoreItemDetailsActivity;
import com.tarungoyaldev.android.noquestore.StoreService;
import com.tarungoyaldev.android.noquestore.barcode.BarcodeCaptureActivity;
import com.tarungoyaldev.android.noquestore.model.LocalStoreProduct;
import com.tarungoyaldev.android.noquestore.model.StoreProduct;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.inject.Inject;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 *
 */
public class StoreItemListFragment extends ListFragment {

    public interface StoreProductUpdateListener {
        void addStoreProduct(StoreProduct storeProduct);
        void storeListFetched();
    }

    private static final String TAG = "StoreFinderFragment";
    private StoreProductUpdateListener storeProductUpdateListener;

    @Inject
    Config config;
    @Inject
    StoreService storeService;

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ((StoreItemListActivity) getActivity()).inject(this);
        List<StoreProduct> storeProducts = new ArrayList<>();
        StoreProductListAdapter adapter = new StoreProductListAdapter(getContext(), storeProducts);
        setListAdapter(adapter);
    }

    @Override
    public void onResume() {
        super.onResume();
        Observable<List<StoreProduct>> storeProListObservable  =
                storeService.getStoreProductList(config.getStoreId());

        storeProductUpdateListener = (StoreProductUpdateListener) getActivity();

        storeProListObservable.subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(storeProductList -> {
//                    insertStoreProductsInDB(storeProductList);
                    StoreProductListAdapter storeProductListAdapter = (StoreProductListAdapter) getListAdapter();
                    storeProductListAdapter.clear();
                    storeProductListAdapter.addItems(storeProductList);
                    storeProductUpdateListener.storeListFetched();
                }, error -> {
                    Log.e(TAG, error.toString());
                });
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        StoreProduct storeProduct = ((StoreProductListAdapter) getListAdapter()).getItem(position);
        Intent intent = new Intent(getContext(), StoreItemDetailsActivity.class);
        intent.putExtra(BarcodeCaptureActivity.BarcodeObject,
                String.valueOf(storeProduct.getProduct().getBarcodeNumber()));
        startActivity(intent);
    }

    private void insertStoreProductsInDB(List<StoreProduct> storeProductList) {
        Set<String> barcodeSet = new HashSet<>();
        for (LocalStoreProduct localStoreProduct : LocalStoreProduct.listAll(LocalStoreProduct.class)) {
            barcodeSet.add(localStoreProduct.getBarcode());
        }
        for (StoreProduct storeProduct : storeProductList) {
            if (!barcodeSet.contains(String.valueOf(storeProduct.getProduct().getBarcodeNumber()))) {
                LocalStoreProduct localStoreProduct = new LocalStoreProduct(
                        storeProduct.getProduct().getProductName(),
                        storeProduct.getStorePrice().intValue(),
                        storeProduct.getCostPrice().intValue(),
                        storeProduct.getQuantityAvailable().intValue(),
                        String.valueOf(storeProduct.getProduct().getBarcodeNumber()));
                localStoreProduct.save();
            }
        }
    }
}