package com.tarungoyaldev.android.noquestore.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by tarungoyal on 05/08/16.
 */
public class Order implements Serializable {

    @SerializedName("data")
    @Expose
    private Data data;
    @SerializedName("message")
    @Expose
    private String message;

    /**
     *
     * @return
     * The data
     */
    public Data getData() {
        return data;
    }

    public List<StoreProduct> getStoreProducts() {
        ArrayList<StoreProduct> storeProducts = new ArrayList<>();
        for (Data.OrderItem orderItem: data.getOrderItems()) {
            StoreProduct storeProduct = orderItem.getStoreProduct();
            storeProduct.setQuantityAvailable(orderItem.getQuantity());
            storeProducts.add(storeProduct);
        }
        return storeProducts;
    }

    /**
     *
     * @param data
     * The data
     */
    public void setData(Data data) {
        this.data = data;
    }

    /**
     *
     * @return
     * The message
     */
    public String getMessage() {
        return message;
    }

    /**
     *
     * @param message
     * The message
     */
    public void setMessage(String message) {
        this.message = message;
    }

    public class Data {

        @SerializedName("order_items")
        @Expose
        private List<OrderItem> orderItems = new ArrayList<OrderItem>();
        @SerializedName("payment_status")
        @Expose
        private String paymentStatus;
        @SerializedName("store_id")
        @Expose
        private Long storeId;

        /**
         *
         * @return
         * The orderItems
         */
        public List<OrderItem> getOrderItems() {
            return orderItems;
        }

        /**
         *
         * @param orderItems
         * The Order items
         */
        public void setOrderItems(List<OrderItem> orderItems) {
            this.orderItems = orderItems;
        }

        /**
         *
         * @return
         * The paymentStatus
         */
        public String getPaymentStatus() {
            return paymentStatus;
        }

        /**
         *
         * @param paymentStatus
         * The payment_status
         */
        public void setPaymentStatus(String paymentStatus) {
            this.paymentStatus = paymentStatus;
        }

        /**
         *
         * @return
         * The storeId
         */
        public Long getStoreId() {
            return storeId;
        }

        /**
         *
         * @param storeId
         * The store_id
         */
        public void setStoreId(Long storeId) {
            this.storeId = storeId;
        }

        public class OrderItem implements Serializable {

            @SerializedName("quantity")
            @Expose
            private Long quantity;
            @SerializedName("store_product")
            @Expose
            private StoreProduct storeProduct;

            /**
             *
             * @return
             * The quantity
             */
            public Long getQuantity() {
                return quantity;
            }

            /**
             *
             * @param quantity
             * The quantity
             */
            public void setQuantity(Long quantity) {
                this.quantity = quantity;
            }

            /**
             *
             * @return
             * The storeProduct
             */
            public StoreProduct getStoreProduct() {
                return storeProduct;
            }

            /**
             *
             * @param storeProduct
             * The store_product
             */
            public void setStoreProduct(StoreProduct storeProduct) {
                this.storeProduct = storeProduct;
            }

        }
    }
}
