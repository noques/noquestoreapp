package com.tarungoyaldev.android.noquestore.StoreItemDetails;

import com.tarungoyaldev.android.noquestore.AppModule;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by tarungoyal on 12/07/16.
 */
@Module(
        injects = {
                StoreItemDetailsActivity.class,
        },
        library = true,
        addsTo = AppModule.class
)
public class StoreItemDetailsModule {
    private StoreItemDetailsActivity storeItemDetailsActivity;

    public StoreItemDetailsModule(StoreItemDetailsActivity storeItemDetailsActivity) {
        this.storeItemDetailsActivity = storeItemDetailsActivity;
    }

    @Provides
    @Singleton
    public StoreItemDetailsActivity provideStoreItemDetailsActivity() {
        return storeItemDetailsActivity;
    }
}
