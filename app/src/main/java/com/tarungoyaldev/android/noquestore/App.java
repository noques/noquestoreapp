package com.tarungoyaldev.android.noquestore;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.multidex.MultiDex;

import com.orm.SugarApp;
import com.tarungoyaldev.android.noquestore.model.StoreLogin;

import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;

import dagger.ObjectGraph;

/**
 * Created by tarungoyal on 11/07/16.
 */
public class App extends SugarApp {

    private ObjectGraph objectGraph;
    @Inject Config config;

    @Override public void onCreate() {
        super.onCreate();
        objectGraph = ObjectGraph.create(getModules().toArray());
        objectGraph.inject(this);
        System.out.println("In App.java, Config username: " + config.getBaseUrl());
    }

    @Override
    protected void attachBaseContext(Context context) {
        super.attachBaseContext(context);
        MultiDex.install(this);
    }

    private List<Object> getModules() {
        return Arrays.<Object>asList(new AppModule(this));
    }

    public ObjectGraph createScopedGraph(Object... modules) {
        return objectGraph.plus(modules);
    }

    public void setStoreManager(StoreLogin storeLogin) {
        config.setStoreLogin(storeLogin);
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(getString(R.string.auth_token), storeLogin.getToken());
        editor.putLong(getString(R.string.store_id), storeLogin.getStoreId());
        editor.putLong(getString(R.string.user_id), storeLogin.getUserId());
        editor.apply();
    }

    /**
     * Initiate {@link StoreLogin} from {@link SharedPreferences}
     * @return whether store details were successfully retrieved.
     */
    public boolean initiateStoreLogin() {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
        StoreLogin storeLogin = new StoreLogin(
                sharedPref.getString(getString(R.string.auth_token), null),
                sharedPref.getLong(getString(R.string.user_id), 0),
                sharedPref.getLong(getString(R.string.store_id), 0)
        );
        if (storeLogin.getStoreId() != (long) 0) {
            config.setStoreLogin(storeLogin);
            return true;
        } else {
            return false;
        }
    }

    public void logout() {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.remove(getString(R.string.auth_token));
        editor.remove(getString(R.string.user_id));
        editor.remove(getString(R.string.store_id));
        editor.apply();
        config.setStoreLogin(null);
    }
}
