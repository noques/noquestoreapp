package com.tarungoyaldev.android.noquestore.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by tarungoyal on 12/07/16.
 */
public class StoreProduct implements Serializable {
    @SerializedName("product")
    @Expose
    private Product product;
    @SerializedName("store")
    @Expose
    private StoreInformation store;
    @SerializedName("cost_price")
    @Expose
    private Long costPrice;
    @SerializedName("store_price")
    @Expose
    private Long storePrice;
    @SerializedName("quantity_available")
    @Expose
    private Long quantityAvailable;
    @SerializedName("par_level")
    @Expose
    private Long parLevel;
    @SerializedName("discount")
    @Expose
    private Double discount;
    @SerializedName("imageurl")
    @Expose
    private Object imageurl;

    /**
     *
     * @return
     * The product
     */
    public Product getProduct() {
        return product;
    }

    /**
     *
     * @param product
     * The product
     */
    public void setProduct(Product product) {
        this.product = product;
    }

    /**
     *
     * @return
     * The store
     */
    public StoreInformation getStore() {
        return store;
    }

    /**
     *
     * @param store
     * The store
     */
    public void setStore(StoreInformation store) {
        this.store = store;
    }

    /**
     *
     * @return
     * The costPrice
     */
    public Long getCostPrice() {
        return costPrice;
    }

    /**
     *
     * @param costPrice
     * The cost_price
     */
    public void setCostPrice(Long costPrice) {
        this.costPrice = costPrice;
    }

    /**
     *
     * @return
     * The storePrice
     */
    public Long getStorePrice() {
        return storePrice;
    }

    /**
     *
     * @param storePrice
     * The store_price
     */
    public void setStorePrice(Long storePrice) {
        this.storePrice = storePrice;
    }

    /**
     *
     * @return
     * The quantityAvailable
     */
    public Long getQuantityAvailable() {
        return quantityAvailable;
    }

    /**
     *
     * @param quantityAvailable
     * The quantity_available
     */
    public void setQuantityAvailable(Long quantityAvailable) {
        this.quantityAvailable = quantityAvailable;
    }

    /**
     *
     * @return
     * The parLevel
     */
    public Long getParLevel() {
        return parLevel;
    }

    /**
     *
     * @param parLevel
     * The par_level
     */
    public void setParLevel(Long parLevel) {
        this.parLevel = parLevel;
    }

    /**
     *
     * @return
     * The discount
     */
    public Double getDiscount() {
        return discount;
    }

    /**
     *
     * @param discount
     * The discount
     */
    public void setDiscount(Double discount) {
        this.discount = discount;
    }

    /**
     *
     * @return
     * The imageurl
     */
    public Object getImageurl() {
        return imageurl;
    }

    /**
     *
     * @param imageurl
     * The imageurl
     */
    public void setImageurl(Object imageurl) {
        this.imageurl = imageurl;
    }
}
