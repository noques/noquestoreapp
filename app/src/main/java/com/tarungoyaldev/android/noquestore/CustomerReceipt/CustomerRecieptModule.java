package com.tarungoyaldev.android.noquestore.CustomerReceipt;

import com.tarungoyaldev.android.noquestore.AppModule;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by tarungoyal on 12/07/16.
 */
@Module(
        injects = {
                CustomerReceiptActivity.class,
                ReceiptItemListFragment.class,
        },
        library = true,
        addsTo = AppModule.class
)
public class CustomerRecieptModule {
    private CustomerReceiptActivity customerReceiptActivity;

    public CustomerRecieptModule(CustomerReceiptActivity customerRecieptActivity) {
        this.customerReceiptActivity = customerRecieptActivity;
    }

    @Provides
    @Singleton
    public CustomerReceiptActivity provideCustomerRecieptActivity() {
        return customerReceiptActivity;
    }
}
