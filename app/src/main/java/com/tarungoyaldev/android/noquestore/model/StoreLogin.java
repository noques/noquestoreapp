package com.tarungoyaldev.android.noquestore.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by tarungoyal on 04/08/16.
 */
public class StoreLogin {

    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("user_id")
    @Expose
    private Long userId;
    @SerializedName("store_id")
    @Expose
    private Long storeId;
    @SerializedName("token")
    @Expose
    private String token;

    public StoreLogin(String token, Long userId, Long storeId) {
        this.token = token;
        this.userId = userId;
        this.storeId = storeId;
    }
    /**
     *
     * @return
     * The message
     */
    public String getMessage() {
        return message;
    }

    /**
     *
     * @param message
     * The message
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     *
     * @return
     * The userId
     */
    public Long getUserId() {
        return userId;
    }

    /**
     *
     * @param userId
     * The user_id
     */
    public void setUserId(Long userId) {
        this.userId = userId;
    }

    /**
     *
     * @return
     * The storeId
     */
    public Long getStoreId() {
        return storeId;
    }

    /**
     *
     * @param storeId
     * The store_id
     */
    public void setStoreId(Long storeId) {
        this.storeId = storeId;
    }

    /**
     *
     * @return
     * The token
     */
    public String getToken() {
        return token;
    }

    /**
     *
     * @param token
     * The token
     */
    public void setToken(String token) {
        this.token = token;
    }
}
