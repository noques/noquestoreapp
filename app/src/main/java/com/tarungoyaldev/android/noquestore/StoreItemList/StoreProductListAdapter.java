package com.tarungoyaldev.android.noquestore.StoreItemList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.tarungoyaldev.android.noquestore.R;
import com.tarungoyaldev.android.noquestore.model.StoreProduct;

import java.util.List;

/**
 * Created by tarungoyal on 12/07/16.
 */
public class StoreProductListAdapter extends ArrayAdapter<StoreProduct> {
    private final Context context;
    private final List<StoreProduct> storeProducts;

    public StoreProductListAdapter(Context context, List<StoreProduct> storeProducts) {
        super(context, -1, storeProducts);
        this.context = context;
        this.storeProducts = storeProducts;
        setNotifyOnChange(true);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        StoreProduct storeProduct = storeProducts.get(position);

        View rowView = inflater.inflate(R.layout.store_product_list_item, parent, false);
        TextView itemName = (TextView) rowView.findViewById(R.id.listItemName);
        TextView itemSellingPrice = (TextView) rowView.findViewById(R.id.listItemSellingPrice);
        TextView itemQuantity = (TextView) rowView.findViewById(R.id.listItemQuantity);
        ImageView itemImageView = (ImageView) rowView.findViewById(R.id.listProductImageView);

        itemName.setText(storeProduct.getProduct().getProductName());
        itemSellingPrice.setText("Store price: " + String.valueOf(storeProduct.getStorePrice()));
        itemQuantity.setText("Quantity: " + String.valueOf(storeProduct.getQuantityAvailable()));
        Picasso.with(context)
                .load(storeProduct.getProduct().getImageUrl())
                .placeholder(R.drawable.placeholder)
                .into(itemImageView);
        return rowView;
    }

    public void addItems(List<StoreProduct> storeProducts) {
        this.storeProducts.addAll(storeProducts);
        notifyDataSetChanged();
    }
}
