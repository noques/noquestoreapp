package com.tarungoyaldev.android.noquestore.Login;

import com.tarungoyaldev.android.noquestore.AppModule;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by tarungoyal on 12/07/16.
 */
@Module(
        injects = {
                LoginActivity.class,
        },
        addsTo = AppModule.class
)
public class LoginModule {
    private LoginActivity loginActivity;

    public LoginModule(LoginActivity loginActivity) {
        this.loginActivity = loginActivity;
    }

    @Provides
    @Singleton
    public LoginActivity provideLoginActivity() {
        return loginActivity;
    }
}
