package com.tarungoyaldev.android.noquestore.CustomerReceipt;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.tarungoyaldev.android.noquestore.R;
import com.tarungoyaldev.android.noquestore.model.StoreProduct;

import java.util.List;

/**
 * Created by tarungoyal on 12/07/16.
 */
public class RecieptItemListAdapter extends ArrayAdapter<StoreProduct> {
    private final Context context;
    private final List<StoreProduct> storeProducts;

    public RecieptItemListAdapter(Context context, List<StoreProduct> storeProducts) {
        super(context, -1, storeProducts);
        this.context = context;
        this.storeProducts = storeProducts;
        setNotifyOnChange(true);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        StoreProduct storeProduct = storeProducts.get(position);

        View rowView = inflater.inflate(R.layout.reciept_list_item, parent, false);
        TextView itemName = (TextView) rowView.findViewById(R.id.productName);
        TextView itemSellingPrice = (TextView) rowView.findViewById(R.id.productSellingPrice);
        TextView itemQuantity = (TextView) rowView.findViewById(R.id.productQuantity);
        ImageView itemImageView = (ImageView) rowView.findViewById(R.id.productImage);

        itemName.setText(storeProduct.getProduct().getProductName());
        itemSellingPrice.setText(String.valueOf(storeProduct.getStorePrice()));
        itemQuantity.setText(String.valueOf(storeProduct.getQuantityAvailable()));
        Picasso.with(context)
                .load(storeProduct.getProduct().getImageUrl())
                .placeholder(R.drawable.placeholder)
                .into(itemImageView);
        return rowView;
    }

    public void addItems(List<StoreProduct> storeProducts) {
        this.storeProducts.addAll(storeProducts);
        notifyDataSetChanged();
    }
}
