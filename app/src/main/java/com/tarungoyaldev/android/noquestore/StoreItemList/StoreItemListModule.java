package com.tarungoyaldev.android.noquestore.StoreItemList;

import com.tarungoyaldev.android.noquestore.AppModule;
import com.tarungoyaldev.android.noquestore.model.StoreProduct;

import java.util.ArrayList;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by tarungoyal on 12/07/16.
 */
@Module(
        injects = {
                StoreItemListActivity.class,
                StoreItemListFragment.class,
        },
        library = true,
        addsTo = AppModule.class
)
public class StoreItemListModule {
    private StoreItemListActivity storeItemListActivity;

    public StoreItemListModule(StoreItemListActivity storeItemListActivity) {
        this.storeItemListActivity = storeItemListActivity;
    }

    @Provides
    @Singleton
    public StoreItemListActivity provideStoreItemListActivity() {
        return storeItemListActivity;
    }

    @Provides
    @Singleton
    public ArrayList<StoreProduct> provideStoreProductsList() {
        return new ArrayList<StoreProduct>();
    }
}
