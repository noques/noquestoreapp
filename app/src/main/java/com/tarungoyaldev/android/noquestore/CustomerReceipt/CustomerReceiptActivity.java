package com.tarungoyaldev.android.noquestore.CustomerReceipt;

import android.os.Bundle;
import android.widget.TextView;

import com.tarungoyaldev.android.noquestore.Config;
import com.tarungoyaldev.android.noquestore.DaggerActivity;
import com.tarungoyaldev.android.noquestore.R;
import com.tarungoyaldev.android.noquestore.StoreService;

import javax.inject.Inject;

public class CustomerReceiptActivity extends DaggerActivity
        implements ReceiptItemListFragment.ReceiptUpdateListener{

    @Inject
    StoreService storeService;
    @Inject
    Config config;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_customer_receipt);
        super.onCreate(savedInstanceState);

        if (findViewById(R.id.reciptItemListContainer) != null) {

            if (savedInstanceState != null) {
                return;
            }

            ReceiptItemListFragment receiptItemListFragment = new ReceiptItemListFragment();

            receiptItemListFragment.setArguments(getIntent().getExtras());

            getSupportFragmentManager().beginTransaction()
                    .add(R.id.reciptItemListContainer, receiptItemListFragment).commit();
        }
    }

    @Override
    protected Object getModule() {
        return new CustomerRecieptModule(this);
    }

    @Override
    public void updateTotalAmount(int amount) {
        TextView totalAmountTextView = (TextView) findViewById(R.id.totalAmountTextView);
        totalAmountTextView.setText(String.valueOf(amount));
    }
}
