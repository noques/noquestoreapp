package com.tarungoyaldev.android.noquestore;

import com.tarungoyaldev.android.noquestore.StoreItemDetails.StoreItemDetailsActivity;
import com.tarungoyaldev.android.noquestore.model.Message;
import com.tarungoyaldev.android.noquestore.model.Order;
import com.tarungoyaldev.android.noquestore.model.StoreInformation;
import com.tarungoyaldev.android.noquestore.model.StoreLogin;
import com.tarungoyaldev.android.noquestore.model.StoreProduct;

import java.util.HashMap;
import java.util.List;

import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import rx.Observable;

/**
 * Created by tarungoyal on 11/07/16.
 */
public interface StoreService {
    @GET("noque/api/?")
    Observable<List<StoreInformation>> getStoreList();

    @GET("noque/api/{storeId}")
    Observable<StoreInformation> getStoreInformation(@Path("storeId") long storeId);

    @GET("noque/api/store_products/{storeId}/{barcode}/?")
    Observable<StoreProduct> getStoreProduct(@Path("storeId") long storeId,
                                             @Path("barcode") String barcode);

    @GET("noque/api/store_products/{storeId}/?")
    Observable<List<StoreProduct>> getStoreProductList(@Path("storeId") long storeId);

    @GET("noque/orders/{orderId}")
    Observable<Order> getOrder(@Path("orderId") String orderId);

    @POST("store/api/login/")
    Observable<StoreLogin> loginStoreManager(@Body HashMap<String, String> loginBody);

    @POST("noque/api/store/store_product_list/")
    Observable<Message> addStoreProducts(
            @Body StoreItemDetailsActivity.StoreProductUpdateBody storeProductUpdateBody);
}
