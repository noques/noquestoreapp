package com.tarungoyaldev.android.noquestore;

import android.content.Context;

import com.google.gson.Gson;
import com.tarungoyaldev.android.noquestore.model.StoreInformation;

import java.io.IOException;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by tarungoyal on 11/07/16.
 */
@Module(
        injects = {
                App.class,
                Config.class
        },
        library = true
)
public class AppModule {

    private App app;

    public AppModule(App app) {
        this.app = app;
    }

    @Provides
    @Singleton
    public Context provideApplicationContext() {
        return app;
    }

    @Provides
    @Singleton
    public Config provideConfig() {
        return new Config();
    }

    @Provides
    @Singleton
    public StoreService provideStoreService(Config config) {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Interceptor.Chain chain) throws IOException {
                Request original = chain.request();

                Request.Builder requestBuilder = original.newBuilder()
                        .header("Content-Type", "application/json")
                        .method(original.method(), original.body());

                if (config.getStoreLogin() != null && config.getStoreLogin().getToken() != null) {
                    requestBuilder.header("authorization", "Token " + config.getStoreLogin().getToken());
                }

                Request request = requestBuilder.build();

                return chain.proceed(request);
            }
        });
        httpClient.addInterceptor(logging);  // <-- this is the important line!

        OkHttpClient client = httpClient.build();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(config.getBaseUrl())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();

        return retrofit.create(StoreService.class);
    }

    @Provides
    @Singleton
    public StoreInformation provideStoreInformation() {
        return new StoreInformation();
    }

    @Provides
    @Singleton
    public Gson provideGson() {
        return new Gson();
    }
}
