package com.tarungoyaldev.android.noquestore.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by tarungoyal on 11/07/16.
 */
public class StoreInformation implements Serializable{
    @SerializedName("id")
    @Expose
    private Long id;
    @SerializedName("store_name")
    @Expose
    private String storeName;
    @SerializedName("location")
    @Expose
    private String location;
    @SerializedName("city")
    @Expose
    private String city;
    @SerializedName("phone")
    @Expose
    private Long phone;
    @SerializedName("pincode")
    @Expose
    private Long pincode;

    public void setStoreInformation(StoreInformation information) {
        id = information.getId();
        storeName = information.getStoreName();
        location = information.getLocation();
        city = information.getCity();
        phone = information.getPhone();
        pincode = information.getPincode();
    }

    /**
     *
     * @return
     * The id
     */
    public Long getId() {
        return id;
    }

    /**
     *
     * @param id
     * The id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     *
     * @return
     * The storeName
     */
    public String getStoreName() {
        return storeName;
    }

    /**
     *
     * @param storeName
     * The store_name
     */
    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    /**
     *
     * @return
     * The location
     */
    public String getLocation() {
        return location;
    }

    /**
     *
     * @param location
     * The location
     */
    public void setLocation(String location) {
        this.location = location;
    }

    /**
     *
     * @return
     * The city
     */
    public String getCity() {
        return city;
    }

    /**
     *
     * @param city
     * The city
     */
    public void setCity(String city) {
        this.city = city;
    }

    /**
     *
     * @return
     * The phone
     */
    public Long getPhone() {
        return phone;
    }

    /**
     *
     * @param phone
     * The phone
     */
    public void setPhone(Long phone) {
        this.phone = phone;
    }

    /**
     *
     * @return
     * The pincode
     */
    public Long getPincode() {
        return pincode;
    }

    /**
     *
     * @param pincode
     * The pincode
     */
    public void setPincode(Long pincode) {
        this.pincode = pincode;
    }
}
