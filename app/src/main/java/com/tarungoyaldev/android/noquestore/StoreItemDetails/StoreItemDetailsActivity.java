package com.tarungoyaldev.android.noquestore.StoreItemDetails;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.annotations.SerializedName;
import com.squareup.picasso.Picasso;
import com.tarungoyaldev.android.noquestore.Config;
import com.tarungoyaldev.android.noquestore.DaggerActivity;
import com.tarungoyaldev.android.noquestore.R;
import com.tarungoyaldev.android.noquestore.StoreService;
import com.tarungoyaldev.android.noquestore.barcode.BarcodeCaptureActivity;
import com.tarungoyaldev.android.noquestore.model.Message;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class StoreItemDetailsActivity extends DaggerActivity {

    private EditText itemNameEditText;
    private EditText itemCostPriceEditText;
    private EditText itemSellingPriceEditText;
    private EditText itemQuantityEditText;
    private String barcode;
//    private LocalStoreProduct localStoreProduct = null;

    @Inject
    Config config;
    @Inject
    StoreService storeService;

    @Override
    protected Object getModule() {
        return new StoreItemDetailsModule(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_store_item_details);
        super.onCreate(savedInstanceState);

        barcode = getIntent().getStringExtra(BarcodeCaptureActivity.BarcodeObject);
        System.out.println("Barcode is: " + barcode);

        itemNameEditText = (EditText) findViewById(R.id.itemName);
        itemSellingPriceEditText = (EditText) findViewById(R.id.itemSellingPrice);
        itemCostPriceEditText = (EditText) findViewById(R.id.itemCostPrice);
        itemQuantityEditText = (EditText) findViewById(R.id.itemQuantity);
        TextView barcodeTextView = (TextView) findViewById(R.id.itemBarcode);
        barcodeTextView.setText(barcode);

        storeService.getStoreProduct(config.getStoreId(), barcode)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(storeProduct -> {
                    itemNameEditText.setText(storeProduct.getProduct().getProductName());
                    itemSellingPriceEditText.setText(String.valueOf(storeProduct.getStorePrice()));
                    itemCostPriceEditText.setText(String.valueOf(storeProduct.getCostPrice()));
                    itemQuantityEditText.setText(String.valueOf(storeProduct.getQuantityAvailable()));
                    Picasso.with(this)
                            .load(storeProduct.getProduct().getImageUrl())
                            .placeholder(R.drawable.placeholder)
                            .into((ImageView) findViewById(R.id.itemImage));
                }, error -> {
                    Toast.makeText(this, error.toString(), Toast.LENGTH_LONG).show();
                });
//        List<LocalStoreProduct> localStoreProductList =
//                LocalStoreProduct.find(LocalStoreProduct.class, "barcode = ?", barcode);
//        if (localStoreProductList.size() > 0) {
//            localStoreProduct = localStoreProductList.get(0);
//            itemNameEditText.setText(localStoreProduct.getName());
//            itemSellingPriceEditText.setText(String.valueOf(localStoreProduct.getSellingPrice()));
//            itemCostPriceEditText.setText(String.valueOf(localStoreProduct.getSellingPrice()));
//            itemQuantityEditText.setText(String.valueOf(localStoreProduct.getQuantity()));
//            barcodeTextView.setText(localStoreProduct.getBarcode());
//        }
    }

    public void saveStoreItem(View view) {
//        if (localStoreProduct != null) {
//            localStoreProduct.setName(itemNameEditText.getText().toString());
//            localStoreProduct.setSellingPrice(
//                    Integer.valueOf(itemSellingPriceEditText.getText().toString()));
//            localStoreProduct.setCostPrice(
//                    Integer.valueOf(itemCostPriceEditText.getText().toString()));
//            localStoreProduct.setQuantity(
//                    Integer.valueOf(itemQuantityEditText.getText().toString()));
//            localStoreProduct.save();
//        } else {
//            localStoreProduct = new LocalStoreProduct(
//                    itemNameEditText.getText().toString(),
//                    Integer.valueOf(itemSellingPriceEditText.getText().toString()),
//                    Integer.valueOf(itemCostPriceEditText.getText().toString()),
//                    Integer.valueOf(itemQuantityEditText.getText().toString()),
//                    barcode
//            );
//            localStoreProduct.save();
//        }
        storeService.addStoreProducts(getStoreProductUpdateBody())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.newThread())
                .subscribe(message -> {
                    if (message.getMessage().equals(Message.SUCCESS_PRODUCT_ADDED)) {
                        Toast.makeText(this, Message.SUCCESS_PRODUCT_ADDED, Toast.LENGTH_LONG).show();
                        finish();
                    } else {
                        Toast.makeText(this, message.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }, error -> {
                    Toast.makeText(this, error.toString(), Toast.LENGTH_LONG).show();
                });
    }

    public StoreProductUpdateBody getStoreProductUpdateBody() {
        return new StoreProductUpdateBody(String.valueOf(config.getStoreId()),
                itemNameEditText.getText().toString(),
                itemCostPriceEditText.getText().toString(),
                itemSellingPriceEditText.getText().toString(),
                itemQuantityEditText.getText().toString(),
                barcode);
    }

    public class StoreProductUpdateBody implements Serializable {

        @SerializedName("store_id")
        String storeId;

        @SerializedName("store_products")
        List<StoreProductApi> storeProducts;

        public StoreProductUpdateBody(String storeId, String productName, String costPrice,
                                      String storePrice, String quantityAvailable, String barcodeNumber) {
            this.storeId = storeId;
            storeProducts = new ArrayList<>();
            storeProducts.add(new StoreProductApi(productName, costPrice, storePrice,
                    quantityAvailable, barcodeNumber));
        }

        public class StoreProductApi implements Serializable {
            @SerializedName("product_name")
            String productName;
            @SerializedName("cost_price")
            String costPrice;
            @SerializedName("store_price")
            String storePrice;
            @SerializedName("quantity_available")
            String quantityAvailable;
            @SerializedName("barcode_number")
            String barcodeNumber;

            public StoreProductApi(String productName, String costPrice, String storePrice,
                                   String quantityAvailable, String barcodeNumber) {
                this.productName = productName;
                this.costPrice = costPrice;
                this.storePrice = storePrice;
                this.quantityAvailable = quantityAvailable;
                this.barcodeNumber = barcodeNumber;
            }
        }
    }
}
